package draycott.live.demo.controllers

import draycott.live.demo.tools.TestTools
import draycott.live.services.UserServiceImpl
import draycott.live.services.UserToolsServiceImpl
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration
@WebAppConfiguration
@SpringBootTest
class LoginControllerTest {

    @Autowired
    private lateinit var context: WebApplicationContext
    @Autowired
    private lateinit var userService: UserServiceImpl
    @Autowired
    private lateinit var userToolsService: UserToolsServiceImpl

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity())
                .build()
    }

    @Test
    fun loginPageTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.view().name("/login"))
    }

    @Test
    fun loginFormTest() {
        TestTools.performTestUserLogin(userToolsService, userService, mockMvc)
    }

    @Test
    fun logoutTest() {
        mockMvc.perform(logout())
    }
}