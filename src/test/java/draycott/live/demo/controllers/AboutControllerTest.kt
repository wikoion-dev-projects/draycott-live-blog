package draycott.live.demo.controllers

import draycott.live.controllers.AboutController
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.view
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.view.InternalResourceViewResolver

@RunWith(MockitoJUnitRunner::class)
@ContextConfiguration
@SpringBootTest
class AboutControllerTest {

    private lateinit var mockMvc: MockMvc

    @InjectMocks
    private lateinit var aboutController: AboutController

    @Before
    fun setup() {
        val viewResolver = InternalResourceViewResolver()
        viewResolver.setPrefix("/templates/")
        viewResolver.setSuffix(".html")

        mockMvc = MockMvcBuilders.standaloneSetup(aboutController).setViewResolvers(viewResolver).build()
    }

    @Test
    @Throws(Exception::class)
    fun testAboutPage() {
        mockMvc.perform(get("/about"))
                .andExpect(status().isOk)
                .andExpect(view().name("about"))
    }
}
