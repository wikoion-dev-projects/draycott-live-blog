package draycott.live.demo.controllers

import draycott.live.controllers.RegistrationController
import draycott.live.demo.tools.TestTools
import draycott.live.models.RegistrationForm
import draycott.live.repositories.UserRepository
import draycott.live.services.IsUniqueEmailValidator
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.model
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@RunWith(MockitoJUnitRunner::class)
@ContextConfiguration
@SpringBootTest
class RegistrationControllerPageTest {

    @Mock
    private lateinit var isUniqueEmailValidator: IsUniqueEmailValidator
    @Mock
    private lateinit var userRepository: UserRepository
    @Mock
    private lateinit var registrationForm: RegistrationForm
    @InjectMocks
    private lateinit var registrationController: RegistrationController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(registrationController)
                .setViewResolvers(TestTools.createViewResolver()).build()
    }

    @Test
    fun testRegistrationPage() {
        mockMvc.perform(get("/register"))
                .andExpect(status().isOk)
                .andExpect(model().attributeExists("registrationForm"))
    }

}