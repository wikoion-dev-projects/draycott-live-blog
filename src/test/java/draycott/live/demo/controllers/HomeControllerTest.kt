package draycott.live.demo.controllers

import draycott.live.controllers.HomeController
import draycott.live.services.PostService
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders


@RunWith(MockitoJUnitRunner::class)
@ContextConfiguration
@SpringBootTest
class HomeControllerTest {

    @Mock
    private lateinit var postService: PostService
    @InjectMocks
    private lateinit var homeController: HomeController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(homeController).build()
    }

    @Test
    @Throws(Exception::class)
    fun testHomePage() {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk)
                .andExpect(view().name("index"))
                .andExpect(model().attributeExists("latest3posts", "latest5posts"))
    }

}
