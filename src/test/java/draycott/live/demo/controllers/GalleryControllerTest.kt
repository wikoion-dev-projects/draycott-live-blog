package draycott.live.demo.controllers

import draycott.live.controllers.GalleryController
import draycott.live.demo.tools.TestTools
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@RunWith(MockitoJUnitRunner::class)
@ContextConfiguration
@SpringBootTest
class GalleryControllerTest {

    @InjectMocks
    private lateinit var galleryController: GalleryController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(galleryController)
                .setViewResolvers(TestTools.createViewResolver()).build()
    }

    @Test
    fun testGalleryPage() {
        mockMvc.perform(get("/gallery"))
                .andExpect(status().isOk)
    }
}
