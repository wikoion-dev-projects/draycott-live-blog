package draycott.live.demo.controllers

import draycott.live.config.StorageProperties
import draycott.live.controllers.FileViewController
import draycott.live.demo.tools.TestTools
import draycott.live.services.StorageServiceImpl
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration
@SpringBootTest
class FileViewControllerTest {

    @Autowired
    private lateinit var storageService: StorageServiceImpl
    @Autowired
    private lateinit var storageProperties: StorageProperties

    private lateinit var fileViewController: FileViewController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        fileViewController = FileViewController(storageService)

        mockMvc = MockMvcBuilders.standaloneSetup(fileViewController)
                .setViewResolvers(TestTools.createViewResolver()).build()
    }

    @Test
    fun testFileView() {
        val filename = "fileUploadTestImage.png"
        val file = MockMultipartFile("file", filename,
                MediaType.IMAGE_PNG_VALUE, "test data".toByteArray())

        val rootLocation = Paths.get(storageProperties.location)
        val inputStream = file.inputStream

        Files.copy(inputStream, rootLocation.resolve(filename),
                StandardCopyOption.REPLACE_EXISTING)

        mockMvc.perform(get("/files/$filename"))
                .andExpect(status().isOk)

        //Cleanup created test file
        Files.delete(rootLocation.resolve(filename))
    }

}