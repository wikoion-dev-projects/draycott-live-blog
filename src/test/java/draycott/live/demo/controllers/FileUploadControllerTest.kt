package draycott.live.demo.controllers

import draycott.live.controllers.FileUploadController
import draycott.live.demo.tools.TestTools
import draycott.live.services.RequestToolsService
import draycott.live.services.StorageService
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@RunWith(MockitoJUnitRunner::class)
@ContextConfiguration
@SpringBootTest
class FileUploadControllerTest {

    @Mock
    private lateinit var requestToolsService: RequestToolsService
    @Mock
    private lateinit var storageService: StorageService
    @InjectMocks
    private lateinit var fileUploadController: FileUploadController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(fileUploadController)
                .setViewResolvers(TestTools.createViewResolver()).build()
    }

    @Test
    fun testFileUpload() {
        val file = MockMultipartFile("file",
                "fileUploadTestImage.png",
                MediaType.IMAGE_PNG_VALUE, "test data".toByteArray())

        mockMvc.perform(MockMvcRequestBuilders.multipart("/file-upload")
                .file(file))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }
}