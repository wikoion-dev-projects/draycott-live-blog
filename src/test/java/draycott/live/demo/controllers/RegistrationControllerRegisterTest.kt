package draycott.live.demo.controllers

import org.hamcrest.Matchers.containsString
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.view
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration
@WebAppConfiguration
@SpringBootTest
class RegistrationControllerRegisterTest {

    @Autowired
    private lateinit var wac: WebApplicationContext

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity())
                .build()
    }

    @Test
    fun testRegistration() {
        mockMvc.perform(post("/register")
                .with(csrf())
                .param("username", "test")
                .param("password", "test")
                .param("confirmPassword", "test")
                .param("fullname", "test")
                .param("email", "testRegistration@test.com"))

                .andExpect(MockMvcResultMatchers.status().isFound)
    }

    @Test
    fun testRegistrationWithPasswordMismatch() {
        mockMvc.perform(post("/register")
                .with(csrf())
                .param("username", "test")
                .param("password", "test")
                .param("confirmPassword", "test2")
                .param("fullname", "test")
                .param("email", "testRegistration@test.com"))

                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(view().name("registration"))
                .andExpect(content().string(containsString("Error")))
    }

    @WithMockUser(roles = ["ADMIN"])
    @Test
    fun testRegistrationWithAuthenticatedUser() {
        mockMvc.perform(post("/register")
                .with(csrf())
                .param("username", "test")
                .param("password", "test")
                .param("confirmPassword", "test")
                .param("fullname", "test")
                .param("email", "testRegistration@test.com"))

                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }
}