package draycott.live.demo.controllers

import draycott.live.demo.tools.TestTools
import draycott.live.repositories.UserRepository
import draycott.live.services.UserServiceImpl
import draycott.live.services.UserToolsServiceImpl
import org.hamcrest.CoreMatchers.containsString
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration
@WebAppConfiguration
@SpringBootTest
class UserAreaControllerTest {

    @Autowired
    private lateinit var wac: WebApplicationContext
    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var userService: UserServiceImpl
    @Autowired
    private lateinit var userToolsService: UserToolsServiceImpl

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity())
                .build()
    }

    @Test
    fun testUserAreaPageUnauthenticated() {
        mockMvc.perform(get("/user-area").with(csrf()))
                .andExpect(redirectedUrlPattern("**/login"))
    }

    @WithMockUser(username = "TestUser", roles = ["USER"])
    @Test
    fun testUserAreaPage() {
        TestTools.performTestUserLogin(userToolsService, userService, mockMvc)

        mockMvc.perform(get("/user-area")
                .with(csrf()))
                .andExpect(status().isOk)
                .andExpect(view().name("/user-area"))
                .andExpect(model().attributeExists("changeEmailForm"))
                .andExpect(model().attributeExists("changePasswordForm"))
    }

    @WithMockUser(username = "TestUser", roles = ["USER"])
    @Test
    fun testChangePassword() {
        TestTools.performTestUserLogin(userToolsService, userService, mockMvc)

        mockMvc.perform(post("/change-password")
                .with(csrf())
                .param("currentPassword", "TestUserPassword")
                .param("password", "TestUserNewPassword")
                .param("confirmPassword", "TestUserNewPassword"))
                .andExpect(status().isOk)
    }

    @WithMockUser(username = "TestUser", roles = ["USER"])
    @Test
    fun testChangeNotMatchingPassword() {
        TestTools.performTestUserLogin(userToolsService, userService, mockMvc)

        mockMvc.perform(post("/change-password")
                .with(csrf())
                .param("currentPassword", "TestUserPassword")
                .param("password", "TestUserNewPassword")
                .param("confirmPassword", "TestUserNewPassword2"))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString(
                        "Error: The password fields must match")))
    }

    @WithMockUser(username = "TestUser", roles = ["USER"])
    @Test
    fun testChangeInvalidPassword() {
        TestTools.performTestUserLogin(userToolsService, userService, mockMvc)

        mockMvc.perform(post("/change-password")
                .with(csrf())
                .param("currentPassword", "TestUserInvalidPassword")
                .param("password", "TestUserNewPassword")
                .param("confirmPassword", "TestUserNewPassword"))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString(
                        "Error: Incorrect password")))
    }

    @WithMockUser(username = "TestUser", roles = ["USER"])
    @Test
    fun testChangeEmail() {
        TestTools.performTestUserLogin(userToolsService, userService, mockMvc)

        mockMvc.perform(post("/change-email")
                .with(csrf())
                .param("currentEmailPassword", "TestUserPassword")
                .param("newEmailAddress", "TestUserEmail@ChangedTestUserEmail.com"))
                .andExpect(status().isOk)
    }

    @WithMockUser(username = "TestUser", roles = ["USER"])
    @Test
    fun testChangeEmailWithIncorrectPassword() {
        TestTools.performTestUserLogin(userToolsService, userService, mockMvc)

        mockMvc.perform(post("/change-email")
                .with(csrf())
                .param("currentEmailPassword", "TestUserInvalidPassword")
                .param("newEmailAddress", "TestUserEmail@ChangedTestUserEmail.com"))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString(
                        "Error: Incorrect password")))
    }

}