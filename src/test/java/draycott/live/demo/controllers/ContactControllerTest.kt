package draycott.live.demo.controllers

import draycott.live.controllers.ContactController
import draycott.live.demo.tools.TestTools
import draycott.live.models.ContactForm
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@RunWith(MockitoJUnitRunner::class)
@ContextConfiguration
@SpringBootTest
class ContactControllerTest {

    @Mock
    private lateinit var contactForm: ContactForm
    @InjectMocks
    private lateinit var contactController: ContactController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(contactController)
                .setViewResolvers(TestTools.createViewResolver()).build()
    }

    @Test
    fun testContactPage() {

        mockMvc.perform(get("/contact"))
                .andExpect(status().isOk)
                .andExpect(view().name("contact"))
                .andExpect(model().attributeExists("contactForm"))
    }

    @Test
    fun testContactForm() {
        mockMvc.perform(post("/thanks")
                .param("name", "test")
                .param("email", "test@test.com")
                .param("subject", "test")
                .param("message", "test"))

                .andExpect(status().isOk)
                .andExpect(view().name("/thanks"))
    }

}