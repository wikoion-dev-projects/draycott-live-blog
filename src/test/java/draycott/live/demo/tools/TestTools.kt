package draycott.live.demo.tools

import draycott.live.models.Post
import draycott.live.models.User
import draycott.live.services.UserServiceImpl
import draycott.live.services.UserToolsServiceImpl
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.servlet.view.InternalResourceViewResolver

class TestTools {

    companion object {

        fun createViewResolver(): InternalResourceViewResolver {
            val viewResolver = InternalResourceViewResolver()
            viewResolver.setPrefix("/templates/")
            viewResolver.setSuffix(".html")

            return viewResolver
        }

        fun createTestPost(user: User? = null): Post {
            return Post("Test Post", "<p>Line #1.</p><p>Test Post</p>", user)
        }

        fun performTestUserLogin(userToolsService: UserToolsServiceImpl
                                 , userService: UserServiceImpl, mockMvc: MockMvc) {
            val password = "TestUserPassword"
            val username = "TestUser"
            val passwordHash = userToolsService.encodePassword(password)

            if (userService.findUserByUsername("TestUser") == null) {
                userService.create(User(username, passwordHash,
                        "TestUser", "TestUserEmail@loginTestUserEmail.com"))
            }

            mockMvc.perform(MockMvcRequestBuilders.post("/login").with(SecurityMockMvcRequestPostProcessors.csrf())
                    .param("username", username)
                    .param("password", password))

                    .andExpect(MockMvcResultMatchers.status().isFound)
        }

    }
}
