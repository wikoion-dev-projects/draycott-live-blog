package draycott.live.demo.models

import draycott.live.models.ChangePasswordForm
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.assertTrue

@RunWith(MockitoJUnitRunner::class)
@SpringBootTest
class ChangePasswordFormTest {

    @InjectMocks
    private lateinit var passwordForm: ChangePasswordForm
    private val testPassword = "TestPassword"

    @Test
    fun testCurrentPasswordField() {
        passwordForm.currentPassword = testPassword
        assertTrue { passwordForm.currentPassword == testPassword }
    }

    @Test
    fun testPassword() {
        passwordForm.password = testPassword
        assertTrue { passwordForm.password == testPassword }
    }

    @Test
    fun testConfirmPassword() {
        passwordForm.confirmPassword = testPassword
        assertTrue { passwordForm.confirmPassword == testPassword }
    }
}