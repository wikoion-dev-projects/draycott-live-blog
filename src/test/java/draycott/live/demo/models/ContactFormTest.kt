package draycott.live.demo.models

import draycott.live.models.ContactForm
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.assertTrue

@RunWith(MockitoJUnitRunner::class)
@SpringBootTest
class ContactFormTest {

    @InjectMocks
    private lateinit var contactForm: ContactForm
    private val testString = "TestString"

    @Test
    fun testEmail() {
        contactForm.email = testString
        assertTrue { contactForm.email == testString }
    }

    @Test
    fun testName() {
        contactForm.name = testString
        assertTrue { contactForm.name == testString }
    }

    @Test
    fun testMessage() {
        contactForm.message = testString
        assertTrue { contactForm.message == testString }
    }

    @Test
    fun testSubject() {
        contactForm.subject = testString
        assertTrue { contactForm.subject == testString }
    }
}