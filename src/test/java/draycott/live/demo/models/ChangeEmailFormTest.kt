package draycott.live.demo.models

import draycott.live.models.ChangeEmailForm
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.assertTrue

@RunWith(MockitoJUnitRunner::class)
@SpringBootTest
class ChangeEmailFormTest {

    @InjectMocks
    private lateinit var changeEmailForm: ChangeEmailForm

    @Test
    fun testCurrentEmailPassword() {
        changeEmailForm.currentEmailPassword = "TestPassword"
        assertTrue { changeEmailForm.currentEmailPassword == "TestPassword" }
    }

    @Test
    fun testEmail() {
        changeEmailForm.newEmailAddress = "TestEmail"
        assertTrue { changeEmailForm.newEmailAddress == "TestEmail" }
    }
}