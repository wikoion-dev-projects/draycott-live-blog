package draycott.live.demo.services

import draycott.live.exceptions.StorageException
import draycott.live.exceptions.StorageFileNotFoundException
import draycott.live.services.StorageService
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.io.File
import java.util.stream.Collectors
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest
class StorageServiceTest {

    @Autowired
    private lateinit var storageService: StorageService
    private val file = MockMultipartFile("file",
            "fileUploadTestImage.png",
            MediaType.IMAGE_PNG_VALUE, "test data".toByteArray())
    private lateinit var testFileName: String

    @Test
    fun testStoreFile() {
        storageService.init()
        testFileName = storageService.store(file)
        assertTrue { File(storageService.load(testFileName).toString()).exists() }
    }

    @Test
    fun testDeleteFile() {
        storageService.init()
        testFileName = storageService.store(file)
        assertTrue { storageService.delete(testFileName) }
    }

    @Test
    fun testDeleteBadFile() {
        storageService.init()
        testFileName = "test"
        assertFalse { storageService.delete(testFileName) }
    }

    @Test
    fun testLoadFile() {
        storageService.init()
        testFileName = storageService.store(file)
        assertTrue { storageService.load(testFileName).toFile().exists() }
    }

    @Test//(expected = StorageFileNotFoundException::class)
    fun testLoadEmptyFile() {
        storageService.init()
        try {
            storageService.loadAsResource("test")
        } catch (t: Throwable) {
            assertTrue { t.toString().isNotEmpty() }
        }
    }

    @Test
    fun testLoadAll() {
        storageService.init()
        testFileName = storageService.store(file)
        assertTrue {
            storageService.loadAll().map { it.toAbsolutePath() }
                    .collect(Collectors.toList()).size > 0
        }
    }

    @Test
    fun testLoadAsResource() {
        storageService.init()
        testFileName = storageService.store(file)
        assertTrue { storageService.loadAsResource(testFileName).isReadable }
    }

    @Test(expected = StorageFileNotFoundException::class)
    fun testLoadAsResourceFails() {
        storageService.init()
        storageService.loadAsResource("test").isReadable
    }

    @Test(expected = StorageException::class)
    fun testDeleteAll() {
        storageService.init()
        testFileName = storageService.store(file)
        storageService.store(file)
        storageService.deleteAll()

        storageService.loadAll()
    }
}