package draycott.live.repositories;

import draycott.live.models.User;
import org.springframework.data.repository.CrudRepository;

//@Repository
public interface UserRepository
        extends CrudRepository<User, Long> {

    User findByUsername(String username);

    User findByEmail(String email);
}
