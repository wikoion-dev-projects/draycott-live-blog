package draycott.live.repositories;

import draycott.live.models.Image;
import org.springframework.data.repository.CrudRepository;

//@Repository
public interface ImageRepository extends CrudRepository<Image, Long> {

    Image findByImageNameContaining(String imageName);
}

