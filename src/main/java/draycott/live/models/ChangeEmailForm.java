package draycott.live.models;

import lombok.Data;
import org.springframework.stereotype.Service;

@Data
@Service
public class ChangeEmailForm {
    private String currentEmailPassword;
    private String newEmailAddress;
}
