package draycott.live.models;

import draycott.live.services.FieldMatch;
import lombok.Data;
import org.springframework.stereotype.Service;

@Data
@Service
@FieldMatch(first = "password", second = "confirmPassword", message = " Error: The password fields must match")
public class ChangePasswordForm {
    private String currentPassword;
    private String password;
    private String confirmPassword;
}
