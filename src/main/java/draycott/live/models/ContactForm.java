package draycott.live.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
public class ContactForm {
    private String name;
    private String email;
    private String subject;
    private String message;
}
