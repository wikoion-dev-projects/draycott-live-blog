package draycott.live.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor()
@Entity
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "image_name", unique = true)
    private String imageName;

    @OneToOne
    private User user;

    @Override
    public String toString() {
        return this.imageName + this.id.toString();
    }
}
