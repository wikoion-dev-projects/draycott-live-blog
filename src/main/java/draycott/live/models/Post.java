package draycott.live.models;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String body;
    private Date date = new Date();

    @ManyToOne(fetch = FetchType.LAZY)
    private User author;

    @ManyToMany
    private Set<Image> images;

    public Post(String title, String body, User author) {
        super();
        this.title = title;
        this.body = body;
        this.author = author;
    }

    @Override
    public String toString() {
        return "Post [id=" + id + ", title=" + title + ", body=" + body + ", author=" + author.getFullName() + ", date=" + date + "]";
    }

}
