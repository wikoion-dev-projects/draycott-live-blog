package draycott.live.models;

import draycott.live.services.FieldMatch;
import draycott.live.services.IsUniqueEmail;
import draycott.live.services.IsUniqueUsername;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Data
@Service
@FieldMatch(first = "password", second = "confirmPassword", message = " Error: The password fields must match")
@IsUniqueUsername(first = "username")
@IsUniqueEmail(first = "email")
public class RegistrationForm {

    private String username;
    private String password;
    private String confirmPassword;
    private String fullname;
    private String email;

    public User toUser(PasswordEncoder passwordEncoder) {
        return new User(username, passwordEncoder.encode(password),
                fullname, email);
    }
}
