package draycott.live.services;

import draycott.live.models.Post;
import draycott.live.models.User;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class FirstBootDataInsert implements ApplicationListener<ContextRefreshedEvent> {

    private final PostService postService;
    private final UserService userService;
    private PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();


    public FirstBootDataInsert(PostService postService, UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }

    public void insertPostData() {
        postService.create(new Post("First Post", "<p>Line #1.</p><p>Line #2</p>", null));
        postService.create(new Post("Forth Post", "<p>Not interesting post</p>", null));
    }

    public void createAdminUser() {
        String passwordHash = this.passwordEncoder.encode("admin");
        userService.create(new User("admin", passwordHash, "admin", "admin@admin.com", "ROLE_ADMIN"));
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        insertPostData();
        createAdminUser();
    }

}
