package draycott.live.services;

import draycott.live.config.StorageProperties;
import draycott.live.exceptions.StorageException;
import draycott.live.exceptions.StorageFileNotFoundException;
import draycott.live.models.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

@Service
public class StorageServiceImpl implements StorageService {
    private final Path rootLocation;
    private final ImageService imageService;

    @Autowired
    public StorageServiceImpl(StorageProperties properties, ImageService imageService) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.imageService = imageService;
    }

    @Override
    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage location", e);
        }
    }

    @Override
    public String store(MultipartFile file) {

        //Create new image in the database
        Image image = imageService.create();

        //Get original filename
        String filename = StringUtils.cleanPath(file.getOriginalFilename());

        //Get file extension
        String extension = filename.substring(filename.indexOf("."));

        //Remove extension from filename
        filename = filename.substring(0, filename.indexOf(".")).trim()
                //then append database ID and extension
                + image.getId().toString() + extension;

        //Set filename in the database for easy finding
        image.setImageName(filename);
        imageService.update(image);


        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }

        return filename;
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {

        Path filePath = rootLocation.resolve(filename);

        if (filePath.toFile().exists()) {
            return filePath;
        } else {
            throw new StorageFileNotFoundException("Failed to read stored files");
        }
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public Boolean delete(String filename) throws Exception {
        try {
            if (new File(rootLocation.resolve(filename).toString()).exists()) {
                FileSystemUtils.deleteRecursively(rootLocation.resolve(filename));
            } else return false;
        } catch (FileNotFoundException e) {
            return false;
        }
        return true;
    }
}
