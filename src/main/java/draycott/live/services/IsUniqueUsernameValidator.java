package draycott.live.services;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsUniqueUsernameValidator implements ConstraintValidator<IsUniqueUsername, Object> {

    private UserService userService;
    private String first;
    private String message;

    @Autowired
    public IsUniqueUsernameValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void initialize(IsUniqueUsername constraintAnnotation) {
        first = constraintAnnotation.first();
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        boolean valid = true;
        try {

            final String firstObj = BeanUtils.getProperty(value, first);
            valid = firstObj != null && !this.userService.usernameAlreadyInUse(firstObj);

        } catch (final Exception ignore) {
            // ignore
        }

        if (!valid) {
            context.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(first)
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }

        return valid;
    }
}
