package draycott.live.services;

import draycott.live.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User loadUserByEmail(String email);

    Boolean usernameAlreadyInUse(String username);

    Boolean emailAlreadyInUse(String email);

    User create(User user);

    void update(User user);

    User findUserByUsername(String username);

    void addAuthority(String role, String username);

    Boolean delete(String username);
}
