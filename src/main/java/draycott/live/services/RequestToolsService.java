package draycott.live.services;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public interface RequestToolsService {
    Optional<String> getPreviousPageByRequest(HttpServletRequest request);
}
