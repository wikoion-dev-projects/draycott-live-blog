package draycott.live.services;

import draycott.live.models.Image;
import draycott.live.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserToolsServiceImpl implements UserToolsService {
    private final UserService userService;
    private PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();


    @Autowired
    public UserToolsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = userService.findUserByUsername(authentication.getName());

        return currentUser;
    }

    @Override
    public void updateUserImage(Image userImage, User currentUser) {
        try {
            currentUser.setUserImage(userImage);
            userService.update(currentUser);
        } catch (NullPointerException e) {
        }
    }

    @Override
    public String encodePassword(String password) {
        return encoder.encode(password);
    }

    @Override
    public void changeUserPassword(String password, User user) {
        user.setPasswordHash(password);
        userService.update(user);
    }

    @Override
    public void changeUserEmail(String email, User user) {
        user.setEmail(email);
        userService.update(user);
    }

    @Override
    public boolean passwordMatch(String password, User user) {
        return encoder.matches(password, user.getPassword());
    }
}
