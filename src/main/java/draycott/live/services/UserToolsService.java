package draycott.live.services;

import draycott.live.models.Image;
import draycott.live.models.User;

public interface UserToolsService {
    User getCurrentUser();

    void updateUserImage(Image userImage, User currentUser);

    String encodePassword(String password);

    void changeUserPassword(String password, User user);

    void changeUserEmail(String email, User user);

    boolean passwordMatch(String password, User user);
}
