package draycott.live.services;

import draycott.live.models.Image;
import draycott.live.repositories.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepo;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepo) {
        this.imageRepo = imageRepo;
    }

    @Override
    public Image findByImageName(String filename) {
        return imageRepo.findByImageNameContaining(filename);
    }

    @Override
    public List<Image> findAll() {
        return (List<Image>) this.imageRepo.findAll();
    }

    @Override
    public Optional<Image> findById(Long id) {
        return this.imageRepo.findById(id);
    }

    @Override
    public Image create() {
        Image image = new Image();
        imageRepo.save(image);
        return image;
    }

    @Override
    public void deleteById(Long id) {
        this.imageRepo.deleteById(id);
    }

    @Override
    public void deleteByName(String filename) {
        this.imageRepo.delete(this.imageRepo.findByImageNameContaining(filename));

    }

    @Override
    public void add(Image image) {
        imageRepo.save(image);
    }

    @Override
    public void update(Image image) {
        imageRepo.save(image);
    }

}
