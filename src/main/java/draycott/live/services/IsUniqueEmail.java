package draycott.live.services;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IsUniqueEmailValidator.class)
@Documented
public @interface IsUniqueEmail {

    String message() default " Error! Email already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String first();
}
