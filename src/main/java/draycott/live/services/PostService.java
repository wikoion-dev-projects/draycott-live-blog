package draycott.live.services;

import draycott.live.models.Image;
import draycott.live.models.Post;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PostService {

    List<Post> findAll();

    List<Post> findLatest5();

    Optional<Post> findById(Long id);

    Post create(Post post);

    Post edit(Post post);

    void deleteById(Long id);

    Set<Image> getImages(Long id);

}
