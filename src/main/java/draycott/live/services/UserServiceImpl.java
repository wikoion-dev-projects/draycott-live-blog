package draycott.live.services;

import draycott.live.models.User;
import draycott.live.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Primary
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepo;

    @Autowired
    public UserServiceImpl(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User loadUserByEmail(String email) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(email);

        if (user != null) {
            return user;
        }
        throw new UsernameNotFoundException(
                "User '" + email + "' not found");
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);

        if (user != null) {
            return user;
        }
        throw new UsernameNotFoundException(
                "User '" + username + "' not found");
    }

    @Override
    public Boolean usernameAlreadyInUse(String username) {
        try {

            loadUserByUsername(username);
            return true;

        } catch (UsernameNotFoundException e) {

            return false;
        }
    }

    @Override
    public Boolean emailAlreadyInUse(String email) {
        try {

            loadUserByEmail(email);
            return true;

        } catch (UsernameNotFoundException e) {

            return false;
        }
    }

    @Override
    public User create(User user) {
        userRepo.save(user);
        return user;
    }

    @Override
    public void update(User user) {
        userRepo.save(user);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public void addAuthority(String role, String username) {
        User user = userRepo.findByUsername(username);
        user.addAuthority(role);
        userRepo.save(user);
    }

    @Override
    public Boolean delete(String username) {
        try {
            userRepo.deleteById(userRepo.findByUsername(username).getId());
        } catch (UsernameNotFoundException e) {
            return false;
        }

        return true;
    }
}
