package draycott.live.services;

import draycott.live.models.Image;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ImageService {

    List<Image> findAll();

    Optional<Image> findById(Long id);

    Image create();

    void deleteById(Long id);

    Image findByImageName(String filename);

    void deleteByName(String filename) throws IOException;

    void add(Image image);

    void update(Image image);
}
