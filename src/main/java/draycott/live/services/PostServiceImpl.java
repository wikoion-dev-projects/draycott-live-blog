package draycott.live.services;

import draycott.live.models.Image;
import draycott.live.models.Post;
import draycott.live.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Primary
public class PostServiceImpl implements PostService {

    private PostRepository postRepo;

    @Autowired
    public PostServiceImpl(PostRepository postRepo) {
        this.postRepo = postRepo;
    }

    @Override
    public List<Post> findAll() {
        return (List<Post>) this.postRepo.findAll();
    }

    @Override
    public List<Post> findLatest5() {
        return this.postRepo.findLatest5Posts(PageRequest.of(0, 5));
    }

    @Override
    public Optional<Post> findById(Long id) {
        return this.postRepo.findById(id);
    }

    @Override
    public Post create(Post post) {
        return this.postRepo.save(post);
    }

    @Override
    public Post edit(Post post) {
        return this.postRepo.save(post);
    }

    @Override
    public void deleteById(Long id) {
        this.postRepo.deleteById(id);
    }

    @Override
    public Set<Image> getImages(Long id) {
        return this.postRepo.findById(id).get().getImages();
    }
}
