package draycott.live.services;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsUniqueEmailValidator implements ConstraintValidator<IsUniqueEmail, Object> {

    private UserService userService;
    private String first;
    private String message;

    @Autowired
    public IsUniqueEmailValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void initialize(IsUniqueEmail constraintAnnotation) {
        first = constraintAnnotation.first();
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        boolean valid = true;
        try {

            final String email = BeanUtils.getProperty(value, first);
            valid = email != null && !this.userService.emailAlreadyInUse(email);

        } catch (final Exception ignore) {
            // ignore
        }

        if (!valid) {
            context.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(first)
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }

        return valid;
    }
}
