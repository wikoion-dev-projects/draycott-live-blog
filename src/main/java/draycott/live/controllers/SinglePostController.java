package draycott.live.controllers;

import draycott.live.models.Post;
import draycott.live.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SinglePostController {

    private PostService postService;

    @Autowired
    public SinglePostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/single-post")
    public String singlePost(@RequestParam(name = "id", required = false) Long postId, Model model) {
        Post post = null;

        if (this.postService.findById(postId).isPresent()) {
            post = this.postService.findById(postId).get();
        }

        model.addAttribute("post", post);
        return "single-post";
    }

}
