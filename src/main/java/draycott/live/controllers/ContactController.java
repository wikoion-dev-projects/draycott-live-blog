package draycott.live.controllers;

import draycott.live.models.ContactForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ContactController {
    private ContactForm contactForm;

    @Autowired
    public ContactController(ContactForm contactForm) {
        this.contactForm = contactForm;
    }

    @GetMapping("/contact")
    public String contact(Model model) {
        model.addAttribute("contactForm", contactForm);
        return "contact";
    }

    @PostMapping("/thanks")
    public String submitContactForm(@ModelAttribute ContactForm contactForm) {
        return "/thanks";
    }

}
