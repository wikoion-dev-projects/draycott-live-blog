package draycott.live.controllers;

import draycott.live.models.ChangeEmailForm;
import draycott.live.models.ChangePasswordForm;
import draycott.live.services.ImageService;
import draycott.live.services.UserToolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserOptionsController {
    private final ImageService imageService;
    private UserToolsService userToolsService;
    private ChangePasswordForm changePasswordForm;
    private ChangeEmailForm changeEmailForm;

    @Autowired
    public UserOptionsController(ImageService imageService, UserToolsService userToolsService,
                                 ChangePasswordForm changePasswordForm,
                                 ChangeEmailForm changeEmailForm) {

        this.imageService = imageService;
        this.userToolsService = userToolsService;
        this.changePasswordForm = changePasswordForm;
        this.changeEmailForm = changeEmailForm;
    }

    @GetMapping("/user-area")
    public String userArea(@ModelAttribute("filename") String filename, Model model) {
        //If image has been uploaded it's name will be forwarded in a flash attribute
        userToolsService.updateUserImage(
                imageService.findByImageName(filename),
                userToolsService.getCurrentUser());

        //Add change password and email forms to model////
        model.addAttribute("changePasswordForm", changePasswordForm);
        model.addAttribute("changeEmailForm", changeEmailForm);

        return "/user-area";
    }

    @PostMapping("/change-password")
    public String changePassword(
            @Valid @ModelAttribute("changePasswordForm") ChangePasswordForm changePasswordForm,
            BindingResult bindingResult, @ModelAttribute ChangeEmailForm changeEmailForm,
            Model model) {

        if (bindingResult.hasErrors()) {
            return "/user-area";
        }

        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                userToolsService.passwordMatch(changePasswordForm.getCurrentPassword(),
                        userToolsService.getCurrentUser())) {

            userToolsService.changeUserPassword(userToolsService
                            .encodePassword(changePasswordForm.getPassword()),
                    userToolsService.getCurrentUser());
        } else {
            model.addAttribute("passwordErrorMessage",
                    " Error: Incorrect password");
        }

        return "/user-area";
    }

    @PostMapping("/change-email")
    public String changeEmail(@ModelAttribute ChangeEmailForm changeEmailForm,
                              @Valid @ModelAttribute("changePasswordForm") ChangePasswordForm changePasswordForm,
                              Model model) {

        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                userToolsService.passwordMatch(changeEmailForm.getCurrentEmailPassword(),
                        userToolsService.getCurrentUser())) {

            userToolsService.changeUserEmail(userToolsService
                            .encodePassword(changeEmailForm.getCurrentEmailPassword()),
                    userToolsService.getCurrentUser());
        } else {
            model.addAttribute("emailErrorMessage",
                    " Error: Incorrect password");
        }

        return "/user-area";
    }

}

