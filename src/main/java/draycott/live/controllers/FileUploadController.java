package draycott.live.controllers;

import draycott.live.services.RequestToolsService;
import draycott.live.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FileUploadController {
    private final StorageService storageService;
    private final RequestToolsService requestToolsService;

    @Autowired
    public FileUploadController(StorageService storageService, RequestToolsService requestToolsService) {
        super();
        this.storageService = storageService;
        this.requestToolsService = requestToolsService;
    }

    @PostMapping("/file-upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes, HttpServletRequest request) {

        storageService.init();
        String filename = storageService.store(file);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");
        redirectAttributes.addFlashAttribute("filename", filename);

        return requestToolsService.getPreviousPageByRequest(request).orElse("/");
    }

}
