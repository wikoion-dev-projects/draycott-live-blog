package draycott.live.controllers;

import draycott.live.models.RegistrationForm;
import draycott.live.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegistrationController {

    private UserRepository userRepo;
    private PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    private RegistrationForm registrationForm;

    @Autowired
    public RegistrationController(
            UserRepository userRepo,
            RegistrationForm registrationForm) {

        this.userRepo = userRepo;
        this.registrationForm = registrationForm;
    }

    @GetMapping()
    public String registerForm(Model model) {
        model.addAttribute("registrationForm", registrationForm);
        return "registration";
    }

    @PostMapping()
    public String processRegistration(
            @Valid @ModelAttribute RegistrationForm registrationForm, Errors errors) {

        if (errors.hasErrors()) {
            return "registration";
        }

        userRepo.save(registrationForm.toUser(encoder));
        return "redirect:/login";
    }
}
