package draycott.live.config;

import draycott.live.controllers.FileViewController;
import draycott.live.models.User;
import draycott.live.repositories.UserRepository;
import draycott.live.services.StorageService;
import draycott.live.services.UserToolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

// Used to provide attributes globally, to all controllers
@ControllerAdvice("draycott.live.controllers")
public class AnnotationAdvice {

    private UserRepository userRepo;
    private UserToolsService userToolsService;
    private StorageService storageService;

    @Autowired
    public AnnotationAdvice(UserRepository userRepo,
                            UserToolsService userToolsService, StorageService storageService) {
        this.userRepo = userRepo;
        this.userToolsService = userToolsService;
        this.storageService = storageService;
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        return this.userRepo.findByUsername(authentication.getName());
    }

    @ModelAttribute("userImage")
    protected String userImage() {

        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {

            if (SecurityContextHolder.getContext().getAuthentication().getName() != "TestUser") {
                try {
                    String filename = userToolsService.getCurrentUser().getUserImage().getImageName();

                    return MvcUriComponentsBuilder.fromMethodName(FileViewController.class,
                            "serveFile", storageService.load(filename)
                                    .getFileName().toString()).build().toString();
                } catch (NullPointerException e) {
                    return null;
                }
            } else return null;
        } else return null;
    }
}
