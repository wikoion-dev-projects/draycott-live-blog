package draycott.live.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;


@Configuration
//@EnableJpaRepositories(basePackageClasses = {UserRepository.class, PostRepository.class, ImageRepository.class}, entityManagerFactoryRef="factoryBean")
public class ThymeleafConfig {

    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }
}
