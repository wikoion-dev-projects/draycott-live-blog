/*-------------------------------------------------------------------------------
    IMAGE PREVIEW
  -------------------------------------------------------------------------------*/

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image-preview')
                .attr('src', e.target.result)
                .width(50)
                .height(50);

        };

        reader.readAsDataURL(input.files[0]);
    }
}

/*-------------------------------------------------------------------------------
  PRE LOADER
-------------------------------------------------------------------------------*/

$(window).load(function () {
    $('.preloader').fadeOut(1000); // set duration in brackets    
});


/*-------------------------------------------------------------------------------
  jQuery Parallax
-------------------------------------------------------------------------------*/

function initParallax() {
    $('#home').parallax("50%", 0.3);

}

initParallax();


/* Back top
-----------------------------------------------*/

$(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
        $('.go-top').fadeIn(200);
    } else {
        $('.go-top').fadeOut(200);
    }
});
// Animate the scroll to top
$('.go-top').click(function (event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: 0}, 300);
})

