# Draycott Live Blog
### Simple Blogging platform written in Java, Spring, front end in Vue and testing suite in Kotlin
** Still a WIP **

### Running instructions
** mvn clean install will build a container image, but this will not work correctly yet becuse it's still using an embedded database **
```
# Clone the repo
git clone git@gitlab.com:wikoion-dev-projects/draycott-live-blog.git
cd draycott-live-blog
mvn clean
mvn spring-boot:run
```